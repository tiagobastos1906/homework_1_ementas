# README #


### Homework 1 – “Ementas” ###

## Funcionalidades Base ##
* Lista de Ementas disponibilizadas pelo *Web Service* de Ementas da UA

* Contém **2 fragmentos**, um para mostrar a lista de ementas de cada dia e outro onde podemos ver os detalhes.

* Pequeno menu com **3 itens**:

    1. Onde é possível atualizar lista;

    2. Onde é possível limpar a lista, com este item é possível provar que o acesso aos menus quando não há rede funciona, podendo o utilizador limpar a lista e tentar atualiza-la no modo offline, sendo carregados os dados em cache;

    3. Onde é possível abrir um mapa mostrando a localização da Universidade de Aveiro.

## Funcionalidades de Valorização implementadas ##
* Existem 2 tipos de *layout* neste projeto, um para telemóveis normais, e um *layout* adicional presente no diretório r**es/layout-large** apropriado para tablets ou dispositivos com um ecrã de maiores dimensões.

* Se o dispositivo móvel não tiver conectividade à Internet, serão disponibilizados dados previamente gravados que permanecem em cache, para isto foi utilizado [SharedPreferences](https://developer.android.com/training/basics/data-storage/shared-preferences.html), porque pareceu uma maneira útil e correta de gravar *Strings* *Json* que contêm as ementas da semana, não havendo assim necessidade para outro tipo de data storing como por exemplo *SQLite3*.

* Criação de um *ListAdapter* personalizado, onde em vez de apenas aparecer uma entrada de texto em cada linha da típica *ListView*, é também mostrado um ícone que dá visualmente a perceção ao utilizador se a cantina está aberta nesse dia ou não.